from django.test import TestCase, Client
from django.urls import resolve
from buatstory6.views import bikinstatus
from buatstory6.views import bikinstatus, redirecting
from django.http import HttpRequest

class UnitTestStory6(TestCase):

	#Test views & urls
	def test_homepage_url_is_exist(self):
		response = Client().get('/statusku/')
		self.assertEqual(response.status_code, 200)

	def test_notexist_url_is_notexist(self):
		response = Client().get('/gaada/')
		self.assertEqual(response.status_code, 404)

	def test_homepage_using_homepage_function(self):
		response = resolve('/statusku/')
		self.assertEqual(response.func, bikinstatus)

	def test_homepage_using_homepage_template(self):
		response = Client().get('/statusku/')
		self.assertTemplateUsed(response, 'mystatus.html')

	def test_landing_page_is_completed(self):
		request = HttpRequest()
		response = bikinstatus(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Halo, apa kabar?', html_response)

	def test_landing_page_title_is_right(self):
		request = HttpRequest()
		response = bikinstatus(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<title>STATUS GUE!</title>', html_response)

	#Redirect landing page
	def test_landing_page_using_redirecting_function(self):
		response = resolve('/')
		self.assertEqual(response.func, redirecting)

	def test_landing_page_redirecting(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 302)

	def test_landing_page_redirected_to_home(self):
		response = Client().get('/')
		self.assertRedirects(response, '/statusku/')

	#SetUp and Models
	def setUp(cls):
		Status.objects.create(status="coba-coba")

	def test_if_models_in_database(self):
		modelsObject = Status.objects.create(status="Halaaaaah")
		count_Object = Status.objects.all().count()
		self.assertEqual(count_Object, 2)

	def test_if_StatusDB_status_is_exist(self):
		modelsObject = Status.objects.get(id=1)
		statusObj = Status._meta.get_field('status').verbose_name
		self.assertEqual(statusObj, 'status')
